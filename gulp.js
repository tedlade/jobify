var gulp = require('gulp'),
    sass = require('gulp-sass'),
    sourcemaps = require('gulp-sourcemaps'),
    autoprefixer = require('gulp-autoprefixer'),
    browserSync = require('browser-sync').create(),
    runSequence = require('run-sequence');

//directories
var scssInput = 'stylesheets/scss/**/*.scss',
    cssOutput = 'stylesheets/css',
    sassOptions = {outputStyle: 'compressed'},
    autoprefixerOptions = {
        browsers: ['last 2 versions', '> 5%', 'Firefox > 20']
    };

gulp.task('browserSync', function() {
    browserSync.init({
        server: {
            baseDir: '.'
          },
    });
});

gulp.task('sass', function(){
    return gulp.src(scssInput)
    // initialize Source maps
        .pipe(sourcemaps.init())
        .pipe(sass(sassOptions)) // Converts Sass to CSS with gulp-sass
        .pipe(autoprefixer(autoprefixerOptions))
        // write to sourcemap files
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest(cssOutput))
        .pipe(browserSync.reload({
            stream: true
          }))
});


gulp.task('watch', ['sass'],function(){
    gulp.watch(scssInput, ['sass']);
});

gulp.task('default', function (callback) {
    runSequence(['watch', 'browserSync'],
      callback
    )
  })